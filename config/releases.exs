# In this file, we load production configuration and secrets
# from environment variables. You can also hardcode secrets,
# although such is generally not recommended and you have to
# remember to add this file to your .gitignore.
import Config

config :my_app, MyApp.Repo,
  hostname: System.fetch_env!("DB_HOST"),
  username: System.fetch_env!("DB_USER"),
  password: System.fetch_env!("DB_PASSWORD"),
  database: System.fetch_env!("DB_INSTANCE"),
  port: String.to_integer(System.get_env("DB_PORT") || "5432"),
  ssl: String.to_existing_atom(System.get_env("DB_SSL_ENABLED") || "true"),
  pool_size: String.to_integer(System.get_env("DB_POOL_SIZE") || "10"),
  show_sensitive_data_on_connection_error:
    String.to_existing_atom(System.get_env("DB_SHOW_SENS_INFO") || "false")

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    raise """
    environment variable SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :my_app, MyAppWeb.Endpoint,
  http: [
    port: String.to_integer(System.get_env("PORT") || "4000"),
    transport_options: [socket_opts: [:inet6]]
  ],
  secret_key_base: secret_key_base,
  server: true

# ## Using releases (Elixir v1.9+)
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start each relevant endpoint:
#
# config :my_app, MyAppWeb.Endpoint, server: true
#
# Then you can assemble a release by calling `mix release`.
# See `mix help release` for more information.
