# MyApp


[![pipeline status](https://gitlab.com/chazwatkins/elixir_phoenix_api_template/badges/master/pipeline.svg)](https://gitlab.com/chazwatkins/elixir_phoenix_api_template/-/commits/master)
[![coverage report](https://gitlab.com/chazwatkins/elixir_phoenix_api_template/badges/master/coverage.svg)](https://gitlab.com/chazwatkins/elixir_phoenix_api_template/-/commits/master)

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
