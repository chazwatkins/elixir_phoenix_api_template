########################################
# 1. Build stage
########################################
FROM elixir:1.10.2-alpine as app_builder

# Set up build environment.
ENV MIX_ENV=prod \
    SECRET_KEY_BASE= \
    DB_HOST= \
    DB_INSTANCE= \
    DB_USER= \
    DB_PASSWORD= \
    DB_SSL_ENABLED= \
    DB_SHOW_SENS_INFO=

# Create the application build directory
RUN mkdir /app
WORKDIR /app

# Install build tools needed in addition to Elixir:
# NodeJS is used for Webpack builds of Phoenix assets.
# Hex and Rebar are needed to get and build dependencies.
RUN apk update \
    && mix local.rebar --force \
    && mix local.hex --force

# Copy over all the necessary application files and directories
COPY config ./config
COPY lib ./lib
COPY priv ./priv
COPY mix.exs .
COPY mix.lock .

# Build the application.
RUN mix do deps.get, compile

# Create the release
RUN mix release

########################################
# 2. Build release image
########################################
FROM alpine:3.11

# Install dependencies. Bash and OpenSSL are required for ERTS.
RUN apk update \
    && apk --no-cache --update add bash openssl

# Copy over the build artifact from step 1 and create a non root user
RUN adduser -S app
WORKDIR /home/app
COPY --from=app_builder /app/_build .
RUN chown -R app: ./prod

# Add the start commands bash file
ADD /scripts/start_commands.sh /scripts/start_commands.sh
RUN chmod +x /scripts/start_commands.sh

# Switch to the non root user
USER app

# set the entrypoint to the start commands script
ENTRYPOINT ["/scripts/start_commands.sh"]